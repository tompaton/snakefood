"""
Test using relative import syntax
"""

from __future__ import print_function

from os.path import *
from testsupport import *


def test_single():
    "Test that relative imports are listed."

    fn = join(data, 'single/a.py')
    print('Testing for: %s' % fn)
    compare_expect(fn.replace('.py', '.expect'), None,
                   'sfood', fn, filterdir=(data, 'ROOT'))

    fn = join(data, 'single/c.py')
    print('Testing for: %s' % fn)
    compare_expect(fn.replace('.py', '.expect'), None,
                   'sfood', fn, filterdir=(data, 'ROOT'))
